FROM node:6.9-slim

COPY package.json package.json

RUN npm install

COPY . . 

EXPOSE 8080

ENV PORT 8080

CMD ["node", "movieData.js"]