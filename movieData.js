var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');
var verifier = require('alexa-verifier');
var app = express();

//check if request has a signaturecertchainurl (this is all for authentication)
app.use(function(req, res, next) {
  if (!req.headers.signaturecertchainurl) {
    return next();
  }

  // mark the request body as already having been parsed so it's ignored by
  // other body parser middlewares
  req._body = true;
  req.rawBody = '';

  //once we get data, add it on to our body
  req.on('data', function(data) {
    return req.rawBody += data;
  });

  //once its all over
  req.on('end', function() {

    var cert_url, er, error, requestBody, signature;

    //parse the raw body
    try {
      req.body = JSON.parse(req.rawBody);
    } catch (error) {
      er = error;
      req.body = {};
    }

    //get the information needed to verify the request
    cert_url = req.headers.signaturecertchainurl;
    signature = req.headers.signature;
    requestBody = req.rawBody;

    //verify the request
    verifier(cert_url, signature, requestBody, function(er) {

      if (er) {
        //if it fails, throw an error and return a failure
        console.error('error validating the alexa cert:', er);
        res.status(401).json({ status: 'failure', reason: er });
      } else {
        //proceed
        console.log("verified!");
        next();
      }

    });
  });

});

app.use(bodyParser.json());

app.get('/healthz', function(req, res) {
    res.sendStatus(200);
});

app.get('/', function(req, res){
	res.send("hello");
});

app.post('/', function(req, res){

	//sample data to make alexa return a response
	var data = {
		"version": "1.0",
		"response": {
			"outputSpeech": {
				"type": "PlainText",
				"text": "Please work!"
			},
			"shouldEndSession": true
		},
		"sessionAttributes": {}
	};

	var info = req.body.request;


    //handle different types of requests
    if(info.type == 'LaunchRequest'){
        data.response.outputSpeech.text = "Welcome to Movie data Skill ";
        data.response.shouldEndSession = false;
        res.send(data);
    }

    else if(info.type=='SessionEndedRequest'){}

    else if(info.type=='IntentRequest'){

		if(info.intent.name=="GetLatestMovies")
		{


	//make a request to the movie api
	        var Datum = Date.now();

	request('https://api.themoviedb.org/3/discover/movie?api_key=c2c2f109328c8071473c2e23faeb1b21&release_date.gte='+Datum, function(error, response, body){

		//check if the request was successful
		if (!error && response.statusCode == 200) {

			//parse the JSON string returned by the request
    		body = JSON.parse(body);
    		 var sentence ="latest movies released this month : " ;
         var j=1;
               for (i=0;i<body.results.length;){
                  sentence+=" "+j+" : "+body.results[i].original_title;
                    i++;
                    j++;



    }
	 //modify the sample data so Alexa can return our response
    		data.response.outputSpeech.text = sentence;

    		//send the data back to the echo
    		res.send(data);
		}



	}); //end of request to movieapi

}else if(info.intent.name=="GetReleasDate"){
	//make a request to the movie api
	        var FilmName = info.intent.slots.Film.value;
	
	request('https://api.themoviedb.org/3/search/movie?api_key=c2c2f109328c8071473c2e23faeb1b21&query='+FilmName, function(error, response, body){

		//check if the request was successful
		if (!error && response.statusCode == 200) {

			//parse the JSON string returned by the request
    		body = JSON.parse(body);
    		 var txt ="release date of "+FilmName+" is "+body.results[0].release_date  ;



	 //modify the sample data so Alexa can return our response
    		data.response.outputSpeech.text = txt;

    		//send the data back to the echo
    		res.send(data);
		}



	});
}else if (info.intent.name=="GetOverview"){
	//make a request to the movie api
	        overview = info.intent.slots.Film.value;
			var txt="";
	request('https://api.themoviedb.org/3/search/movie?api_key=c2c2f109328c8071473c2e23faeb1b21&query='+overview, function(error, response, body){

		//check if the request was successful
		if (!error && response.statusCode == 200) {

			//parse the JSON string returned by the request
    		body = JSON.parse(body);
    		 txt ="overview of "+overview+" is "+body.results[0].overview ;


	 //modify the sample data so Alexa can return our response
    		data.response.outputSpeech.text = txt;

    		//send the data back to the echo
    		res.send(data);
		}

	//if the response was successful
    else if(response.statusCode == 404){
        console.log("there was a problem", response.statusCode);
        console.log("triggered");
        data.response.outputSpeech.text = "Sorry, I didn't quite the film name. Could you repeat that again for me?";
        data.response.shouldEndSession = false;
        res.send(data);
    }

	});
}else{/*data.response.outputSpeech.text = "can you please repeat "
        data.response.shouldEndSession = true;
        res.send(data);*/ sentence="can you please repeat";
        data.response.outputSpeech.text = sentence;

        //send the data back to the echo
        res.send(data);
      }



} //if its an intent request

});
var port = process.env.PORT || 3000;
app.listen(port, function () {

    console.log('Movie Server is up!');

});
